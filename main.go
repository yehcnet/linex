package main

import "gitee.com/yehcnet/linex/linex"

func main() {
	s := linex.New()
	s.GET("/get", func(c *linex.Context) {
		c.String(200, "Hello, path: %s", c.Path)
	})
	s.Run()
}
