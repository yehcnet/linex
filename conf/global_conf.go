package conf

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

/*
	全局配置文件模块
*/

type GlobalObj struct {
	Name    string // 当前服务器名称
	Host    string // 监听ip
	Port    int    // 监听端口
	Version string // 当前Linex的版本号
}

var GlobalObject *GlobalObj // 全局唯一对象

func (g *GlobalObj) Reload() {
	// 从json中加载用于自定义的参数
	data, err := ioutil.ReadFile("conf/linex.json")
	if err != nil {
		log.Println("load json err:", err.Error())
		return
	}
	if err := json.Unmarshal(data, &GlobalObject); err != nil {
		log.Println("data un json err:", err.Error())
		return
	}
}

func init() {
	// 如果配置文件没有加载，则是默认的值
	GlobalObject = &GlobalObj{
		Name:    "Linex Web HTTP",
		Version: "v0.1.1",
		Host:    "0.0.0.0",
		Port:    6789,
	}

	// 尝试用 conf/linex.json 加载一些用户的
	GlobalObject.Reload()
}
