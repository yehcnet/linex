# linex

#### 介绍
基于GO实现的HTTP框架 具有路由分组、中间件、错误恢复等功能
（持续更新中...）

#### 软件架构
- linex/
-     |--linex/
-         |--context.go  // 上下文结构体
-         |--linex.go  // server模块
-         |--logger.go  // 日志中间件模块
-         |--recovery.go  // 错误恢复模块
-         |--router.go // 路由模块
-         |--trie.go// 前缀数保存路由信息
-     |--LICENSE
-     |--go.mod   // 依赖管理

#### 安装教程
- go get gitee.com/yehcnet/linex


#### 使用说明
1.  import "gitee.com/yehcnet/linex/linex"
2. 
```go
    s := linex.Default()
    s.Group("/v1")
    s.GET("/get", func(context *linex.Context) {
        c.String(200, "Hello Linex!")
    })
    s.Run(":9999") // 监听 9999 端口
```
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


